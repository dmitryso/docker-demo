## Starting

At first a little bit about directory sharing: vagrant with docker involves 3 'hops': Mac - Linux VM which hosts the docker - a bunch of docker containers.
So directories are at first shared from Mac to VM and then from VM to docker.
By default your entire home directory is shared. If you store the code
somewhere else then you need to customize it in the `Vagrantfile.host`.

      config.vm.synced_folder "/Users/YouUserName", "/s"

Thus on Linux all your files are available at "/s". You may add more synced folders or set the existing one to something else.
The important point is that all the php/node/nginx projects code should be available on the Linux VM. And even more important the
directory storing this project should be shared too.

The config file *docker_control.yml* stores all the folder mappings from Linux to Docker.

By default the config expects that the current repo is located in your home folder, named 'docker-demo' and thus shared to '/s/docker-demo'.
If that's not true then you need to customize it. But you'll better just place it there at first.

After that start the VM and the containers:

      vagrant up --no-parallel

It will take quite a long time. Once it is up try something like `vagrant status`:

      postgres                  running (docker)
      redis                     running (docker)
      elasticsearch             running (docker)
      beanstalkd                running (docker)
      nodeapp1                  running (docker)
      nodeapp2                  running (docker)
      nodeapp3                  running (docker)
      phpfpm                    running (docker)
      phptools                  stopped (docker)
      nginx                     running (docker)

Also when you run `vagrant global-status` you will see that besides all those docker containers there is a virtualbox VM running.
The VM actually hosts all the containers. And chances are good that at some point you will need to ssh into it
and run some docker commands directly (`vagrant ssh c761691`)

     id       name          provider      state   directory
     --------------------------------------------------------------
     c761691  dockerhost    virtualbox    running /work/docker-demo

Now you can try to access some of those containers

* http://192.168.10.10 — Nginx home
* http://192.168.10.10/app1 — plain html file in a mounted folder
* http://192.168.10.10/nodeapp1 — a node.js app
* http://192.168.10.10/nodeapp2 — a node.js app talking to Redis
* http://192.168.10.10/app1/test.php — a simple php script
* http://192.168.10.10/app2/test.php — a php script which uses postgres
* http://app1.local — works only if you have a dns mapping from app1.local to the VM IP address


## Linking

Every service runs in an independed container. Also there is a container with some PHP command-line tools.

The services are linked in the following way:

* Node & PHP are linked to all the backend services
* Nginx is linked to Node & PHP

The way to access one container from another is to use the ENV variables set by docker.
They look like this:

      POSTGRES_PORT_5432_TCP_ADDR = 172.17.0.1
      POSTGRES_PORT_5432_TCP_PORT = 5432
      REDIS_PORT_6379_TCP_ADDR = 172.17.0.2
      REDIS_PORT_6379_TCP_PORT = 6379

There are variables for all the services and all their ports. But there can be a problem - once you restart a server it typically gets
a new IP address, thus all the services using it should be restarted too. To avoid it you can use the port from a variable with
the well known IP address of the VM hosting the containers.


## Docker Images

The following docker images were used, basically the official Docker images are used for everything but the phptools
and beanstalkd server.

* https://hub.docker.com/_/nginx/
* https://hub.docker.com/_/elasticsearch/
* https://hub.docker.com/_/postgres/
* https://hub.docker.com/_/redis/
* https://hub.docker.com/_/node/
* https://hub.docker.com/_/php/
* https://hub.docker.com/r/darh/php-essentials/
* https://hub.docker.com/r/schickling/beanstalkd

Node, PHP-FPM & Postgres images are customized a little bit
thus the Vagrantfile uses the versions prefixed with 'dsokurenko' instead of the official ones.
Eventually you will need to re-build those images and re-upload to your account.

Run something like

      docker build -t kimmel/php dockerfiles/php
      docker build -t kimmel/node dockerfiles/nodejs
      docker build -t kimmel/postgres dockerfiles/postgres
      docker push kimmel/postgres
      docker push kimmel/php
      docker push kimmel/node

Then replace the "dsokurenko" with "kimmel" in the Vagrantfile.
If you make the images private it will be needed to add DockerHub credentials to the Vagrantfile.

If you want to customize some service then at first you can try to copy some configs from inside the container,
change them, and link them back in the Vagrantfile. This way the I've configured the Nginx.

When you really need to customize the build scripts then you need to create a new Dockerfile derived from the official one.

### Node.js

Node projects expect to see a npm's `package.json` and an `index.js` file in the app dir.

### PHP

You will probably want to customize the PHP-FPM dockerfile, as all those PHP dependencies should be compiled.
The current one installs only PG and Redis drivers.



## Using PHP-CS and PHPUnit

The PHP tools are placed in a separate container, which runs only when you use some tool from it.

Some examples (assumes that a demo app is mounted to /app1):

      vagrant docker-run phptools -- phpcs --standard=Zend /app1/src/Currency.php
      vagrant docker-run phptools -- phpunit  --bootstrap app1/src/autoload.php app1/tests

There is a small issue here: vagrant prepends the machine name to each line outputted, thus the output doesn't look very nice,
but it is still readable. To make it look better either run the container in interacive mode:

      vagrant docker-run -t phptools -- /bin/bash
      # run any commands here

Or login to the host VM and use `docker run` command directly.



## Some Important Points

* Never use `vagrant up` with docker, use either `vagrant up --no-parallel` either `vagrant up <container-name>`.
The vagrant's default parallel running does not work with docker.

* And generally when something is messed up with docker, vagrant typically can't recover from it, a popular issue is when
container exits because of some misconfigured shared folder and vagrant can't restart it. The only thing that can help is logging into the VM,
finding the failing container using `docker ps -a` and removing it using `docker rm <container-name>`.

* Also there is a prominent bug in Docker that prevents non-root containers from writing into virtualbox shared folders.
It can be relevant it you would like to share the postgres data directory with Mac - unfortunatelly it is just not possible now.

* Unless you have `VAGRANT_DEFAULT_PROVIDER=docker` somewhere in your env,
you may need to explicitely set the provider for all vagrant commands, e.g. `vagrant up <container> --provider docker`.

## Docker Controll

There is a small utility named `docker_control.rb` and the `docker_control.yml` file which contains quite a bunch of settings.

The settings are:

* *machine*: Set the VM IP address, memory and core count
* *this_dir_path_in_vm*: The important one — the path to this directory inside the Linux VM.
* *postgres_password*: Sets the password (this is used only once – when the Postgres is initialised)
* *nginx_vhosts*: A pair of a domain name and a path to the VM directory with the host files
* *nginx_subdirs*: Those dirs can be accessed like 192.168.10.10/app1
* *node_apps*: A list of node app which will be mounted at /node-app-name. Each app must have a unique port.
* *php_projects*: Those folder will be monted to the container with php tools, so you can run tests / code coverage here.
* *databases*: A list of databases to create.

If you need to remove a node app than destroy the corresponding container before modifying the config file, e.g.:

    vagrant destroy -f nodeapp1

After editing the config you need to update the VM:

    ./docker_control.rb update
    ./docker_control.rb update-db # (only if you edited the list of databases)

There are some other commands in the *docker_control.rb*, but they mostly duplicate the vagrant commands.

And when something is messed up:

    ./docker_control.rb restart-vm

If it doesn't help then:

    ./docker_control.rb kill-vm

And start from scratch.
