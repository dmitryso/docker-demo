#!/usr/bin/env bash

databases=`cat /databases.list`

echo 'creating user databases'

for database in $databases; do
  if psql --username postgres -c "SELECT datname FROM pg_database WHERE datistemplate = false" | grep "$database"
  then
    echo "database $database exist"
  else
    createdb --username postgres $database
  fi
done
