require 'sinatra'
require 'sinatra/base'
require 'pg'
require 'tilt/erubis'
require 'redis'

set :bind, '0.0.0.0'
set :logging, true

get '/' do
  conn = PGconn.connect hostaddr: ENV['POSTGRES_PORT_5432_TCP_ADDR'], port: ENV['POSTGRES_PORT_5432_TCP_PORT'], dbname: "postgres", user: 'postgres', password: '123456789'
  res = conn.exec("select count(*) from information_schema.tables;")
  @count = res.getvalue(0, 0)

  redis = Redis.new host: ENV['REDIS_PORT_6379_TCP_ADDR'], port: ENV['REDIS_PORT_6379_TCP_PORT'], db: 15
  redis.set("test", "hello world")
  @redis_result = redis.get("test")

  @env_vars = ENV

  system "touch /app/#{Time.now.to_i}.txt"
  @files = Dir.glob('/app/*')

  erb :index
end
