<h1>This is a PHP script from App2</h1>

<h3>ENV</h3>
<pre>
  <?php var_export($_SERVER)?>
</pre>

<h3>Postgres connection</h3>
<pre>
  <?php
    $db_host = getenv('POSTGRES_PORT_5432_TCP_ADDR');
    $db_port = getenv('POSTGRES_PORT_5432_TCP_PORT');
    $db_conn = pg_connect("host=$db_host port=$db_port password=123456789 user=postgres dbname=postgres");

    $db_query = "SELECT datname FROM pg_database WHERE datistemplate = false;";
    $db_result = pg_query($db_conn, $db_query);

    echo "$db_query\n";
    while ($row = pg_fetch_row($db_result)) {
      echo "$row[0]\n";
    }
  ?>
</pre>
