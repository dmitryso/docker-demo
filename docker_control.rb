#!/usr/bin/env ruby -w

abort "You need at least ruby 2.0 to run this (you have #{RUBY_VERSION})!" if RUBY_VERSION < '2.0'

require 'yaml'
require 'pp'

def sh(cmd)
  puts cmd
  system cmd
end


module DockerControl
  module_function

  def settings
    @settings ||= YAML.load_file "docker_control.yml"
  end

  def generate_nginx_node_upstreams
    settings['node_apps'].map do |name, config|
      "upstream #{name}-upstream { server #{name}:#{config['port']}; }"
    end.join("\n")
  end

  def generate_nginx_node_locations
    settings['node_apps'].map do |name, config|
      <<-eos
  location /#{name} {
    rewrite ^/#{name}(.*) /$1 break;
    proxy_pass http://#{name}-upstream;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection 'upgrade';
    proxy_set_header Host $host;
    proxy_cache_bypass $http_upgrade;
  }
      eos
    end.join("\n")
  end

  def generate_nginx_vhosts
    settings['nginx_vhosts'].map do |vhost|
      domain = vhost['domain']
      document_root = "/var/nginx/sites/#{domain}"
      <<-eos
server {
  listen 80;
  server_name #{domain};

  location / {
    root #{document_root};
    index index.html;
  }

  location ~ \.php$ {
    root           html;
    fastcgi_pass   php-fpm;
    fastcgi_index  index.php;
    fastcgi_param  SCRIPT_FILENAME  #{document_root}$fastcgi_script_name;
    include        fastcgi_params;
  }
}
      eos
    end.join("\n")
  end

  def update_nginx_config
    config_path = File.expand_path("#{__dir__}/conf/nginx/default.conf")
    config = File.read(config_path)

    replace_in_text config, 'docker_control node upstreams', generate_nginx_node_upstreams
    replace_in_text config, 'docker_control node locations', generate_nginx_node_locations
    replace_in_text config, 'docker_control vhosts', generate_nginx_vhosts

    File.write(config_path, config)
  end

  def replace_in_text(text, marker, new_content)
    start_tag = "# #{marker} begin"
    end_tag   = "# #{marker} end"
    text.sub!(/(?:#{start_tag}).*(?:#{end_tag})/m, "#{start_tag}\n#{new_content}\n#{end_tag}")
  end

  def start_containers
    sh "vagrant up --no-parallel --provider docker"
  end

  def stop_containers
    sh "vagrant destroy -f"
  end

  def vm_host_id
    `vagrant global-status | grep dockerhost | awk '{print $1}'`.chomp
  end

  def restart_vm
    stop_containers
    host_id = vm_host_id
    if host_id && host_id != ''
      sh "vagrant reload #{vm_host_id}"
      start_containers
    else
      start_containers
    end
  end

  def kill_vm
    host_id = vm_host_id
    if host_id && host_id != ''
      sh "vagrant destroy -f #{vm_host_id}"
    end
  end

  def restart(container_name)
    stop(container_name)
    start(container_name)
  end

  def start(container_name)
    sh "vagrant up --provider docker #{container_name}"
  end

  def stop(container_name)
    sh "vagrant destroy -f #{container_name}"
  end

  def update_databases
    File.write "conf/postgres/databases.list", settings['databases'].join("\n")
    sh %{vagrant ssh #{vm_host_id} -c "docker exec postgres su - postgres -c '/db_control.sh'"}
  end

  def list_databases
    sh %{vagrant ssh #{vm_host_id} -c "docker exec postgres su - postgres -c 'psql -c \\"SELECT datname FROM pg_database WHERE datistemplate = false\\"'"}
  end
end


if $0 =~ /docker_control.rb/
  command = ARGV.first
  case command
  when 'start'
    if ARGV[1]
      DockerControl.start ARGV[1]
    else
      DockerControl.start_containers
    end
  when 'stop'
    if ARGV[1]
      DockerControl.stop ARGV[1]
    else
      DockerControl.stop_containers
    end
  when 'update'
    DockerControl.update_nginx_config
    DockerControl.update_databases
    DockerControl.restart_vm
  when 'update-db'
    DockerControl.update_databases
  when 'list-db'
    DockerControl.list_databases
  when 'restart-vm'
    DockerControl.restart_vm
  when 'kill-vm'
    DockerControl.stop_containers
    DockerControl.kill_vm
  when 'restart'
    abort "Syntax: ./docker_control.rb restart CONTAINER_NAME" unless ARGV[1]
    DockerControl.restart ARGV[1]
  when ''
    puts "No command specified"
  else
    puts "unknown command #{command}"
  end
end
