ENV['VAGRANT_DEFAULT_PROVIDER'] = 'docker'

def docker_container(name, vm_config, options = {})
  vm_config.vm.define name do |container_config|
    container_config.vm.provider "docker" do |d|
      d.vagrant_machine = "dockerhost"
      d.vagrant_vagrantfile = "./Vagrantfile.host"
      d.name = name
      d.remains_running = true

      if options[:link] == :all
        d.link "postgres:postgres"
        d.link "redis:redis"
        d.link "elasticsearch:elasticsearch"
        d.link "beanstalkd:beanstalkd"
      end

      yield d
    end
  end
end

require_relative 'docker_control'

Vagrant.configure(2) do |config|
  docker_container "postgres", config do |d|
    d.image = "dsokurenko/postgres"
    d.ports = ["5432:5432"]
    d.volumes = [
      "/vol/postgres:/pgdata",
      "#{DockerControl.settings['this_dir_path_in_vm']}/conf/postgres/databases.list:/databases.list:ro",
    ]
    d.env = { 'POSTGRES_PASSWORD' => DockerControl.settings['123456789'], 'PGDATA' => "/pgdata/data" }
  end

  docker_container "redis", config do |d|
    d.image = "redis"
  end

  docker_container "elasticsearch", config do |d|
    d.image = "elasticsearch"
  end

  docker_container "beanstalkd", config do |d|
    d.image = "schickling/beanstalkd"
  end

  # docker_container "sinatra", config, link: :all do |d|
  #   d.image = "dsokurenko/test"
  #   d.ports = ["4567:4567"]
  #   d.volumes = ["/s/docker-demo/data/rubyapp:/app/test"]
  # end

  DockerControl.settings['node_apps'].each do |app_name, app_settings|
    docker_container app_name, config, link: :all do |d|
      d.image = "dsokurenko/node"
      d.ports = ["#{app_settings['port']}:#{app_settings['port']}"]
      d.volumes = ["#{app_settings['path']}:/app"]
      d.env = { 'PORT' => app_settings['port'] }
    end
  end

  docker_container "phpfpm", config, link: :all do |d|
    d.image = "dsokurenko/php"
    d.ports = ["9000:9000"]

    nginx_subdirs = DockerControl.settings['nginx_subdirs'].map do |host_name, host_path|
      "#{host_path}:/usr/share/nginx/html/#{host_name}"
    end

    nginx_vhosts = DockerControl.settings['nginx_vhosts'].map do |host_config|
      "#{host_config['path']}:/var/nginx/sites/#{host_config['domain']}"
    end

    d.volumes = nginx_subdirs + nginx_vhosts
  end

  docker_container "phptools", config do |d|
    d.image = "darh/php-essentials"
    d.remains_running = false
    d.volumes = DockerControl.settings['php_projects'].map do |container_path, vm_path|
      "#{vm_path}:#{container_path}"
    end
  end

  docker_container "nginx", config do |d|
    d.image = "nginx"
    d.ports = ["80:80"]

    d.link "phpfpm:phpfpm"

    DockerControl.settings['node_apps'].each do |app_name, _|
      d.link "#{app_name}:#{app_name}"
    end

    nginx_configs = [
      "#{DockerControl.settings['this_dir_path_in_vm']}/conf/nginx/nginx.conf:/etc/nginx/nginx.conf:ro",
      "#{DockerControl.settings['this_dir_path_in_vm']}/conf/nginx/default.conf:/etc/nginx/conf.d/default.conf:ro"
    ]

    nginx_subdirs = DockerControl.settings['nginx_subdirs'].map do |host_name, host_path|
      "#{host_path}:/usr/share/nginx/html/#{host_name}:ro"
    end

    nginx_vhosts = DockerControl.settings['nginx_vhosts'].map do |host_config|
      "#{host_config['path']}:/var/nginx/sites/#{host_config['domain']}"
    end

    d.volumes = nginx_configs + nginx_subdirs + nginx_vhosts
  end
end
